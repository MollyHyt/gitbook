## Mermaid Sequence Diagram

```sequence
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```

```mermaid
sequenceDiagram
Alice ->> Bob: Hello Bob, how are you?
Bob-->>John: How about you John?
Bob--x Alice: I am good thanks!
Bob-x John: I am good thanks!
Note right of John: Bob thinks a long<br/>long time, so long<br/>that the text does<br/>not fit on a row.

Bob-->Alice: Checking with John...
Alice->John: Yes... John, how are you?
```


```mermaid
sequenceDiagram
Alice->John: Hello John, how are you?
loop every minute
    John-->Alice: Great!
end
```



# 生成html文件

將項目搭建服務器，生成html文件

1. 在安裝了 nodejs 10 的前提下安裝 gitbook，這裡用 Node 10 版是為了配合 Gitlab CI

    ```bash
    $ nvm use 10
    $ node -v
    v10.24.1
    $ npm install -g gitbook-cli
    ```

2. Fetch GitBook's latest stable version

    ```bash
    gitbook fetch 3.2.3
    ```

3. 查看gitbook是否安裝成功

    ```bash
    $ gitbook -V
    CLI version: 2.3.2
    GitBook version: 3.2.3
    ```

4. cd 到對應項目，初始化目錄文件

    ```bash
    gitbook init
    ```

5. build gitbook

    ```bash
    gitbook build
    ```

6. 開啟服務器

    ```bash
    gitbook serve
    ```

## 安裝 nvm for macOS

  ```bash
  brew install nvm
  ```

安裝完後，為了讓你可以直接在shell使用nvm指令，
必須在你的 .bash_profile 加入以下這行
（習慣把設定放在.bashrc的人可以把以下的.bash_profile改成.bashrc）

  ```bash
  source $(brew --prefix nvm)/nvm.sh
  ```

記得重新source你的 .bash_profile 來讓設定生效

  ```bash
  . ~/.bash_profile
  ```

zsh 下一直認不得 nvm

  ```bash
  vi .bash_profile
  export NVM_DIR="$HOME/.nvm"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
  
  vi .zshr
  source ~/.bash_profile
  ```

```bash
vm install <version>            # 安裝某個版本的 node
nvm uninstall <version>         # 解除安裝指定版本
nvm use [version]               # 使用某個版本的 node
nvm ls                          # 列出本機已安裝的版本
nvm ls-remote                   # 列出所有可安裝版本

nvm alias default <version>     # 設定預設開啟的 node 版本
nvm --version                   # 查看 nvm 版本
nvm which 8.11.1                # 查看某一版本 nvm 的 PATH

$ node -v
v17.0.1

$ npm -v
8.1.0

$ nvm -v
0.39.1

```
